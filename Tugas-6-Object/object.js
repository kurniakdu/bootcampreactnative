//Soal 1
console.log("* Soal 1 *");
console.log("----------");
function arrayToObject(arr) {
    // Code di sini 
    //var now = new Date();
    //var thisYear = now.getFullYear(); // 2020 (tahun sekarang)
    
    for (i=0;i<arr.length;i++){
        var object={
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],            
        }    
        if(arr[i][3]!=null&&arr[i][3]<2020){
            object.age = 2020-arr[i][3]
        } else {
            object.age = "Invalid Birth Year"
        }
        var name = object.firstName+" "+object.lastName
        console.log((i+1)+". "+name+" :");
        console.log(object);
        console.log();
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

console.log("=================");
console.log();

//Soal 2
console.log("* Soal 2 *");
console.log("----------");
function shoppingTime(memberId, money) {
    // you can only write your code here!
    var list = [];
    var uang = money;
    for (j=0;j<5;j++){
        if (money>1500000){
            list.push("Sepatu Stacattu");
            money -= 1500000;
        } else if (money>500000){
            list.push("Baju Zoro");
            money -= 500000;
        } else if (money>250000){
            list.push("Baju H&N");
            money -= 250000;
        } else if (money>175000){
            list.push("Sweater Unikkloh");
            money -= 175000;
        } else if (money>=50000){
            list.push("Casing Handphone");
            money -= 50000;
            break;
        }
    }
    var sisa = money;
    
    if (memberId==""){
        var text = "Mohon maaf, toko X hanya berlaku untuk member saja"; 
        return text
    } else if (uang<50000) {
        var text = "Mohon maaf, uang tidak cukup";
        return text
    } else if (memberId==null){
        var text = "Mohon maaf, toko X hanya berlaku untuk member saja"; 
        return text
    } else {
        var obj = {
            memberId: memberId,
            money: uang,
            listPurchased: list,
            changeMoney: sisa
        }
    }
    return obj
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
console.log();
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
console.log();
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log();
console.log("=================");
console.log();

//Soal 3
console.log("* Soal 3 *");
console.log("----------");
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    var all = []
    for (k=0;k<arrPenumpang.length;k++){
        var asal = rute.indexOf(arrPenumpang[k][1]);
        var tujuan = rute.indexOf(arrPenumpang[k][2]);
        var jauh = tujuan-asal;
        var oby = {
            penumpang: arrPenumpang[k][0],
            naikDari: arrPenumpang[k][1],
            tujuanAkhir: arrPenumpang[k],
            bayar: jauh*2000,
        }
        all.push(oby);
    }
    return all
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]