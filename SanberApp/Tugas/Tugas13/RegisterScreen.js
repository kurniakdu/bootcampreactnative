import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

export default class App extends React.Component{
    render(){
        return(
            <View style={styles.container}>
                <Image style={styles.logo} source={require('./logo.png')} />
                <View style={styles.body}>
                    <Text style={styles.title}>Register</Text>
                    <View style={styles.input}>
                        <Text style={styles.teks}>Username</Text>
                        <View style={styles.box} />
                        <Text style={styles.teks}>Email</Text>
                        <View style={styles.box} />
                        <Text style={styles.teks}>Password</Text>
                        <View style={styles.box} />
                        <Text style={styles.teks}>Ulangi Password</Text>
                        <View style={styles.box} />
                    </View>
                    <View style={styles.button}>
                        <Text style={styles.daftar}>Daftar</Text>
                        <Text style={styles.teks}>Atau</Text>
                        <Text style={styles.masuk}>Masuk</Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    logo: {
        paddingTop: 60,
        width: 325,
        height: 102,
        margin: 15,
    },
    body: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    title: {
        fontSize: 24,
        color: '#003366',
        fontWeight: 'bold'
    },
    input: {
        margin: 2
    },
    teks: {
        fontSize: 16,
        padding: 2,
        color: '#003366',
        fontWeight: 'bold',
    },
    box: {
        width: 295,
        height: 48,
        borderWidth: 1,
        borderColor: '#003366'
    },
    button:{
        alignItems: 'center',
        paddingVertical: 15
    },
    daftar: {
        fontSize: 24,
        paddingVertical: 5,
        paddingHorizontal: 35,
        color: 'white',
        fontWeight: 'bold',
        backgroundColor: '#003366',
        borderRadius: 15,
        marginBottom: 15
    },
    masuk: {
        fontSize: 24,
        paddingVertical: 5,
        paddingHorizontal: 35,
        color: '#3ec6ff',
        fontWeight: 'bold',
        backgroundColor: 'white',
        borderWidth: 1,
        borderRadius: 15,
        marginTop: 15
    }
})