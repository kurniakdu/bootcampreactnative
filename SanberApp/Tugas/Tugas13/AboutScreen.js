import React from 'react';
import { FlatList, ScrollView, StyleSheet, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export default class App extends React.Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.body}>
                    <Text style={styles.title}>Tentang Saya</Text>
                    <Icon name='md-person-circle' size={200} />
                    <Text style={styles.nama}>Ini Namaku</Text>
                    <Text style={styles.job}>React Native Developer</Text>
                </View>
                <View style={styles.box}>
                    <Text style={styles.category}>Portofolio</Text>
                    <View style={styles.line} />
                    <View style={styles.portofolio}>
                        <View style={styles.skill}>
                            <Icon name='logo-gitlab' size={50}/>
                            <Text style={styles.teks}>@ininamaku</Text>
                        </View>
                        <View style={styles.skill}>
                            <Icon name='logo-github' size={50}/>
                            <Text style={styles.teks}>@ininamaku</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.box}>
                    <Text style={styles.category}>Hubungi Saya</Text>
                    <View style={styles.line} />
                    <View style={styles.contact}>
                        <View style={styles.kontak}>
                            <Icon name='logo-facebook' size={50} />
                            <Text style={styles.teks}>Ini Namaku</Text>
                        </View>
                    </View>
                    <View style={styles.contact}>
                        <View style={styles.kontak}>
                            <Icon name='logo-instagram' size={50} />
                            <Text style={styles.teks}>@ini.nama</Text>
                        </View>
                    </View>
                    <View style={styles.contact}>
                        <View style={styles.kontak}>
                            <Icon name='logo-twitter' size={50} />
                            <Text style={styles.teks}>@ininamaku</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    body: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 15
    },
    title: {
        fontSize: 30,
        color: '#003366',
        fontWeight: 'bold'
    },
    nama: {
        fontSize: 24,
        color: '#003366',
        fontWeight: 'bold'
    },
    job: {
        fontSize: 16,
        color: '#3ec6ff',
        fontWeight: 'bold'
    },
    category: {
        fontSize: 18,
        color: '#003366',
        fontWeight: 'normal'
    },
    teks: {
        fontSize: 16,
        padding: 2,
        color: '#003366',
        fontWeight: 'bold',
    },
    box: {
        width: 350,
        backgroundColor: '#efefef',
        padding: 10,
        borderRadius: 15,
        margin: 5
    },
    line:{
        borderWidth:1,
        margin: 2
    },
    portofolio:{
        justifyContent: 'space-evenly',
        flexDirection: 'row'
    },
    skill: {
        alignItems: 'center',
        paddingVertical: 5
    },
    contact: {
        flexDirection: 'column',
        paddingHorizontal: 50
    },
    kontak: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: 5
    }
})