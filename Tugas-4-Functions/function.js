//Soal 1
console.log("* Soal 1 *");
console.log("----------");

function teriak(){
    return "Halo Sanbers!"
}
console.log(teriak());

console.log();
console.log("====================");
console.log();

//Soal 2
console.log("* Soal 2 *");
console.log("----------");

function kalikan(a,b) {
    return a*b
}

var num1 = 12;
var num2 = 4;
 
var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

console.log();
console.log("====================");
console.log();

//Soal 3
console.log("* Soal 3 *");
console.log("----------");

function introduce(p,q,r,s){
    var text = "Nama saya "+p+", umur saya "+q+" tahun, alamat saya di "+r+", dan saya punya hobby yaitu "+s+"!";
    return text
}

var nama = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(nama, age, address, hobby)
console.log(perkenalan);