//Soal 1
console.log("******* Soal 1 *******\n");

//Release 0
console.log("Release 0");
console.log("---------");
class Animal {
    // Code class di sini
    constructor(name){
        this.name=name;
        this.legs=4;
        this.cold_blooded=false;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

//Release 1
console.log("\nRelease 1");
console.log("---------");

class Ape extends Animal {
    constructor(name){
        super(name);
        this.legs=2;
        this.cold_blooded=false;
    }
    yell(){
        console.log("Auooo");
    }
}

class Frog extends Animal {
    constructor(name){
        super(name);
    }
    jump(){
        console.log("hop hop");
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

//Soal 2
console.log("******* Soal 2 *******\n");

//Function
/*
function Clock({ template }) {
  
  var timer;

  function render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  this.stop = function() {
    clearInterval(timer);
  };

  this.start = function() {
    render();
    timer = setInterval(render, 1000);
  };

}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 
*/

//Class
class Clock {
    // Code di sini
    constructor({template}){
        this.template=template;
    }

    render(){
        this.date=new Date();
        this.hours=this.date.getHours();
        if(this.hours<10) this.hours="0"+this.hours;
        this.mins=this.date.getMinutes();
        if(this.mins<10) this.mins="0"+this.mins;
        this.secs=this.date.getSeconds();
        if(this.secs<10) this.secs="0"+this.secs;
        this.output=this.template;
        this.output.replace('h', this.hours)
        this.output.replace('m', this.mins)
        this.output.replace('s', this.secs)
        console.log(this.output);
        
        //saya bingung karena replacenya tidak berjalan
        //namun tetap saya kumpulkan dahulu, sudah mau jam 9 :(
    }

    stop(){
        clearInterval(this.timer);
    }

    start(){
        this.render();
        this.timer=setInterval(this.render,1000);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  