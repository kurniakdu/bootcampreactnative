//Soal 1
console.log("* Soal 1 *");
console.log("----------");

console.log("LOOPING PERTAMA");
var x = 1;
while(x<=20){
    if(x%2==0){
        console.log(x+" - I love coding");
    }
    ++x;
}

console.log();
console.log("LOOPING KEDUA");
var y = 20;
while(y>1){
    if(y%2==0){
        console.log(y+" - I will become a mobile developer");
    }
    --y;
}

console.log();
console.log("======================");
console.log();

//Soal 2
console.log("* Soal 2 *");
console.log("----------");

for(z=1;z<=20;z++){
    if(z%2==0){
        console.log(z+" - Berkualitas");
    } else if(z%3==0){
        console.log(z+" - I Love Coding");
    } else {
        console.log(z+" - Santai");
    }
}

console.log();
console.log("======================");
console.log();

//Soal 3
console.log("* Soal 3 *");
console.log("----------");

for(a=0;a<4;a++){
    console.log("########");
}

console.log();
console.log("======================");
console.log();

//Soal 4
console.log("* Soal 4 *");
console.log("----------");

var text = "#";
var plus = "#";
for(b=0;b<7;b++){
    console.log(text)
    text = text.concat(plus);
}

console.log();
console.log("======================");
console.log();

//Soal 5
console.log("* Soal 5 *");
console.log("----------");

var c = 0;
while(c<8){
    if(c%2==0){
        console.log(" # # # #");
    } else {
        console.log("# # # # ");
    }
    ++c;
}