//Soal 1
console.log("* Soal 1 *");
console.log("----------");

function range(startNum, finishNum){
    var array = [];
    if(startNum<finishNum){
        for(i=startNum;i<=finishNum;i++){
            array.push(i);
        }
    } else if (startNum>finishNum){
        for(i=startNum;i>=finishNum;i--){
            array.push(i);
        }
    } else{
        array.push(-1);
    }
    return array
}

console.log(range(1, 10)) ;
console.log(range(1)) ;
console.log(range(11,18)) ;
console.log(range(54, 50)) ;
console.log(range()) ;

console.log();
console.log("==================");
console.log();

//Soal 2
console.log("* Soal 2 *");
console.log("----------");

function rangeWithStep(startNo, finishNo, step){
    var arr = [];
    var j = startNo
    if(startNo<finishNo){
        while(j<=finishNo){
            arr.push(j);
            j+=step;
        }
    } else if (startNo>finishNo){
        while(j>=finishNo){
            arr.push(j);
            j-=step
        }
    } else{
        arr.push(-1);
    }
    return arr
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

console.log();
console.log("==================");
console.log();

//Soal 3
console.log("* Soal 3 *");
console.log("----------");

function sum(start=0, finish=0, steps=1){
    var arraynya = [];
    var j = start
    if(start<finish){
        while(j<=finish){
            arraynya.push(j);
            j+=steps;
        }
    } else if (start>finish){
        while(j>=finish){
            arraynya.push(j);
            j-=steps
        }
    } else{
        arraynya.push(start);
    }
    
    var jumlah=0
    for(k=0;k<arraynya.length;k++){
        jumlah+=arraynya[k];
    }
    return jumlah
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log();
console.log("==================");
console.log();

//Soal 4
console.log("* Soal 4 *");
console.log("----------");

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

for(l=0;l<input.length;l++){
    console.log("Nomor ID:  "+input[l][0]);
    console.log("Nama Lengkap:  "+input[l][1]);
    console.log("TTL:  "+input[l][2]+" "+input[l][3]);
    console.log("Hobi:  "+input[l][4]);
    console.log();
}

console.log("==================");
console.log();

//Soal 5
console.log("* Soal 5 *");
console.log("----------");

function balikKata(text){
    var kata = [];
    for (m=0;m<text.length;m++){
        kata[m]=text[text.length-1-m];
    }
    return kata
    //kenapa tidak boleh pake join? dia jadi terpisah per huruf :(
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log();
console.log("==================");
console.log();

//Soal 6
console.log("* Soal 6 *");
console.log("----------");

function dataHandling2(x){
    var gabungan = x
    gabungan.push("Pria", "SMA Internasional Metro");
    console.log(gabungan);

    var tanggal = gabungan[3]
    var date = tanggal.split("/");
    var bulan = date[1];
    switch(bulan) {
        case "01": bulan="Januari"; break;
        case "02": bulan="Februari"; break;
        case "03": bulan="Maret"; break;
        case "04": bulan="April"; break;
        case "05": bulan="Mei"; break;
        case "06": bulan="Juni"; break;
        case "07": bulan="Juli"; break;
        case "08": bulan="Agustus"; break;
        case "09": bulan="September"; break;
        case "10": bulan="Oktober"; break;
        case "11": bulan="November"; break;
        case "12": bulan="Desember"; break;
    }
    console.log(bulan);
    
    var angka = date.sort(function(value1,value2){ return value2-value1});;
    console.log(angka);

    console.log(tanggal.split("/").join("-"));

    var nama = gabungan[1];
    var name = nama.slice(0,15);
    console.log(name);
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);