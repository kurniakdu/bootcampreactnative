var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

var waktu=10000;

readBooksPromise(waktu,books[0])
    .then(function(sisaWaktu){
        readBooksPromise(sisaWaktu, books[1])
            .then(function(sisaWaktu){
                readBooksPromise(sisaWaktu, books[2])
            })
    })


